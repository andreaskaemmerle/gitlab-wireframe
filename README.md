# GitLab wireframe

Low-fi Sketch wireframe of the GitLab user interface.

![GitLab Sketch wireframe](https://gitlab.com/andreaskaemmerle/gitlab-wireframe/raw/master/progress/gitlab-lofi01.png)